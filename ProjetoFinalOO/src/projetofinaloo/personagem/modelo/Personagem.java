
package projetofinaloo.personagem.modelo;


import javax.swing.ImageIcon;

public class Personagem {
    
    private int moveX;
    private int moveY;
    private int pontuacao;
    private int tentativas;
    private String nome;
      
    //private final JButton backPersonagem;
    //private int spaceX;
    //private int spaceY;
 
    //construtor
    public Personagem(){
        
        this.nome = "Pikachu";
        this.moveX = 760;
        this.moveY = 460;
        this.pontuacao = 0;
        this.tentativas = 5;
              
    }
    
    public Personagem(String nome){
        
        this.nome = nome;
        
    }


    public int getMoveX() {
        return moveX;
    }

    public void setMoveX(int moveX) {
        this.moveX = moveX;
    }

    public int getMoveY() {
        return moveY;
    }

    public void setMoveY(int moveY) {
        this.moveY = moveY;
    }
    
       public int getPontuacao() {
        return pontuacao;
    }

    public void setPontuacao(int pontuacao) {
        this.pontuacao = pontuacao;
    }

    public int getTentativas() {
        return tentativas;
    }

    public void setTentativas(int tentativas) {
        this.tentativas = tentativas;
    }
    
     public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    

   /*
    public int getSpaceX() {
        return spaceX;
    }

    public void setSpaceX(int spaceX) {
        this.spaceX = spaceX;
    }

    public int getSpaceY() {
        return spaceY;
    }

    public void setSpaceY(int spaceY) {
        this.spaceY = spaceY;
    }


   */
    
    
}
