package projetofinaloo;


import java.awt.event.ActionListener;
import javax.swing.JFrame;
import projetofinaloo.personagem.modelo.Personagem;

public class TelaPrincipal extends javax.swing.JFrame implements ActionListener {
    
    Personagem umPersonagem;

    public TelaPrincipal() {
        initComponents();
        //para titulo
        setTitle("Pikachu in... Lost on the Forest");
        //para fechar janela
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //para local da janela
        //setLocationRelativeTo(null);
        setSize(700,500);
      //para não alterar o tamanho da janela
        setResizable(false);
         
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButtonComecar = new javax.swing.JButton();
        jButtonSairJanelaPrincipal = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        getContentPane().setLayout(null);

        jButtonComecar.setBackground(new java.awt.Color(255, 204, 0));
        jButtonComecar.setFont(new java.awt.Font("OCR A Extended", 1, 18)); // NOI18N
        jButtonComecar.setText("Comecar");
        jButtonComecar.addActionListener(this);
        getContentPane().add(jButtonComecar);
        jButtonComecar.setBounds(230, 360, 160, 40);

        jButtonSairJanelaPrincipal.setBackground(new java.awt.Color(255, 204, 0));
        jButtonSairJanelaPrincipal.setFont(new java.awt.Font("OCR A Extended", 1, 18)); // NOI18N
        jButtonSairJanelaPrincipal.setText("Sair");
        jButtonSairJanelaPrincipal.addActionListener(this);
        getContentPane().add(jButtonSairJanelaPrincipal);
        jButtonSairJanelaPrincipal.setBounds(430, 360, 150, 40);

        jLabel2.setBackground(new java.awt.Color(51, 153, 0));
        jLabel2.setFont(new java.awt.Font("OCR A Extended", 1, 36)); // NOI18N
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/projetofinaloo/imagem/PaginaInicialJogoOO.png"))); // NOI18N
        getContentPane().add(jLabel2);
        jLabel2.setBounds(0, 0, 700, 500);

        pack();
    }

    // Code for dispatching events from components to event handlers.

    public void actionPerformed(java.awt.event.ActionEvent evt) {
        if (evt.getSource() == jButtonComecar) {
            TelaPrincipal.this.jButtonComecarActionPerformed(evt);
        }
        else if (evt.getSource() == jButtonSairJanelaPrincipal) {
            TelaPrincipal.this.jButtonSairJanelaPrincipalActionPerformed(evt);
        }
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonComecarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonComecarActionPerformed
        // TODO add your handling code here:
                     
            new JanelaSecundaria().setVisible(true);
            this.dispose();
 
    }//GEN-LAST:event_jButtonComecarActionPerformed

    private void jButtonSairJanelaPrincipalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSairJanelaPrincipalActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_jButtonSairJanelaPrincipalActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new TelaPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    javax.swing.JButton jButtonComecar;
    javax.swing.JButton jButtonSairJanelaPrincipal;
    javax.swing.JLabel jLabel2;
    // End of variables declaration//GEN-END:variables
}
