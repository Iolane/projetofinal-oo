
package projetofinaloo;

import javax.swing.JFrame;
import projetofinaloo.personagem.modelo.Personagem;

public class Fase1 extends javax.swing.JFrame {

    private Personagem personagem;
   
    public Fase1() {
        initComponents();
        //para titulo
        setTitle("Pikachu in... Lost on the Forest");
        //para fechar janela
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(900,530);
        //para não alterar o tamanho da janela
        setResizable(false);
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        JLabelAsh = new javax.swing.JLabel();
        jPanelInformacaoJogador = new javax.swing.JPanel();
        jLabelFotoPerfil = new javax.swing.JLabel();
        jLabelNome = new javax.swing.JLabel();
        jLabelPontuacao = new javax.swing.JLabel();
        jLabelTentativas = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabelTextoNomeJogador = new javax.swing.JLabel();
        jLabelNumeroPontos = new javax.swing.JLabel();
        jLabelNumeroTentativas = new javax.swing.JLabel();
        jLabelPersonagem = new javax.swing.JLabel();
        JLabelTelaLaberinto = new javax.swing.JLabel();
        jPanelComandosMain = new javax.swing.JPanel();
        jButtonPlay = new javax.swing.JButton();
        canvasSequenciasDeMovimentos = new java.awt.Canvas();
        jLabelTituloMain = new javax.swing.JLabel();
        jButtonVirarParaEsquerda = new javax.swing.JButton();
        jButtonSeguirParaFrente = new javax.swing.JButton();
        jButtonVirarParaDireita = new javax.swing.JButton();
        jButtonSeguirParaDireita = new javax.swing.JButton();
        jButtonSeguirParaEsquerda = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        JLabelAsh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/projetofinaloo/imagem/ash.png"))); // NOI18N
        getContentPane().add(JLabelAsh);
        JLabelAsh.setBounds(400, 120, 42, 80);

        jPanelInformacaoJogador.setBackground(new java.awt.Color(0, 0, 0));
        jPanelInformacaoJogador.setMinimumSize(new java.awt.Dimension(400, 200));
        jPanelInformacaoJogador.setLayout(null);

        jLabelFotoPerfil.setIcon(new javax.swing.ImageIcon(getClass().getResource("/projetofinaloo/imagem/pikachu-perfil.gif"))); // NOI18N
        jPanelInformacaoJogador.add(jLabelFotoPerfil);
        jLabelFotoPerfil.setBounds(0, 70, 180, 130);

        jLabelNome.setFont(new java.awt.Font("OCR A Extended", 1, 18)); // NOI18N
        jLabelNome.setForeground(new java.awt.Color(255, 255, 255));
        jLabelNome.setText("NOME:");
        jPanelInformacaoJogador.add(jLabelNome);
        jLabelNome.setBounds(180, 80, 60, 20);

        jLabelPontuacao.setBackground(new java.awt.Color(0, 0, 0));
        jLabelPontuacao.setFont(new java.awt.Font("OCR A Extended", 1, 18)); // NOI18N
        jLabelPontuacao.setForeground(new java.awt.Color(255, 255, 255));
        jLabelPontuacao.setText("PONTUACAO:");
        jPanelInformacaoJogador.add(jLabelPontuacao);
        jLabelPontuacao.setBounds(180, 120, 120, 20);

        jLabelTentativas.setBackground(new java.awt.Color(0, 0, 0));
        jLabelTentativas.setFont(new java.awt.Font("OCR A Extended", 1, 18)); // NOI18N
        jLabelTentativas.setForeground(new java.awt.Color(255, 255, 255));
        jLabelTentativas.setText("TENTAIVAS:");
        jPanelInformacaoJogador.add(jLabelTentativas);
        jLabelTentativas.setBounds(180, 160, 120, 20);

        jLabel5.setBackground(new java.awt.Color(0, 0, 0));
        jLabel5.setFont(new java.awt.Font("OCR A Extended", 1, 24)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Informacoes do Jogador");
        jPanelInformacaoJogador.add(jLabel5);
        jLabel5.setBounds(30, 20, 360, 38);

        jLabelTextoNomeJogador.setBackground(new java.awt.Color(0, 0, 0));
        jLabelTextoNomeJogador.setFont(new java.awt.Font("OCR A Extended", 1, 18)); // NOI18N
        jLabelTextoNomeJogador.setForeground(new java.awt.Color(255, 255, 255));
        jLabelTextoNomeJogador.setText("PIKACHU");
        jPanelInformacaoJogador.add(jLabelTextoNomeJogador);
        jLabelTextoNomeJogador.setBounds(240, 80, 110, 20);

        jLabelNumeroPontos.setBackground(new java.awt.Color(0, 0, 0));
        jLabelNumeroPontos.setFont(new java.awt.Font("OCR A Extended", 1, 18)); // NOI18N
        jLabelNumeroPontos.setForeground(new java.awt.Color(255, 255, 255));
        jLabelNumeroPontos.setText("00");
        jPanelInformacaoJogador.add(jLabelNumeroPontos);
        jLabelNumeroPontos.setBounds(310, 120, 24, 20);

        jLabelNumeroTentativas.setBackground(new java.awt.Color(0, 0, 0));
        jLabelNumeroTentativas.setFont(new java.awt.Font("OCR A Extended", 1, 18)); // NOI18N
        jLabelNumeroTentativas.setForeground(new java.awt.Color(255, 255, 255));
        jLabelNumeroTentativas.setText("05");
        jPanelInformacaoJogador.add(jLabelNumeroTentativas);
        jLabelNumeroTentativas.setBounds(310, 160, 34, 20);

        getContentPane().add(jPanelInformacaoJogador);
        jPanelInformacaoJogador.setBounds(0, 0, 400, 200);

        jLabelPersonagem.setBackground(new java.awt.Color(255, 255, 255));
        jLabelPersonagem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/projetofinaloo/imagem/Teste1.gif"))); // NOI18N
        getContentPane().add(jLabelPersonagem);
        jLabelPersonagem.setBounds(760, 460, 30, 30);

        JLabelTelaLaberinto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/projetofinaloo/imagem/NewFase1.png"))); // NOI18N
        getContentPane().add(JLabelTelaLaberinto);
        JLabelTelaLaberinto.setBounds(400, 0, 500, 500);

        jPanelComandosMain.setBackground(new java.awt.Color(0, 0, 0));
        jPanelComandosMain.setMaximumSize(new java.awt.Dimension(50, 50));
        jPanelComandosMain.setMinimumSize(new java.awt.Dimension(50, 50));
        jPanelComandosMain.setPreferredSize(new java.awt.Dimension(50, 50));

        jButtonPlay.setBackground(new java.awt.Color(0, 0, 0));
        jButtonPlay.setIcon(new javax.swing.ImageIcon(getClass().getResource("/projetofinaloo/imagem/iconePlay.png"))); // NOI18N
        jButtonPlay.setPreferredSize(new java.awt.Dimension(50, 50));
        jButtonPlay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPlayActionPerformed(evt);
            }
        });

        canvasSequenciasDeMovimentos.setBackground(new java.awt.Color(51, 51, 51));

        jLabelTituloMain.setBackground(new java.awt.Color(0, 0, 0));
        jLabelTituloMain.setFont(new java.awt.Font("OCR A Extended", 1, 18)); // NOI18N
        jLabelTituloMain.setForeground(new java.awt.Color(255, 255, 255));
        jLabelTituloMain.setText("MAIN:");

        jButtonVirarParaEsquerda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/projetofinaloo/imagem/NewSetaParaesquerda.png"))); // NOI18N
        jButtonVirarParaEsquerda.setPreferredSize(new java.awt.Dimension(50, 50));

        jButtonSeguirParaFrente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/projetofinaloo/imagem/NewSetaParaFrente.png"))); // NOI18N
        jButtonSeguirParaFrente.setPreferredSize(new java.awt.Dimension(50, 50));
        jButtonSeguirParaFrente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSeguirParaFrenteActionPerformed(evt);
            }
        });

        jButtonVirarParaDireita.setIcon(new javax.swing.ImageIcon(getClass().getResource("/projetofinaloo/imagem/NewSetaParaDireita.png"))); // NOI18N
        jButtonVirarParaDireita.setPreferredSize(new java.awt.Dimension(50, 50));

        jButtonSeguirParaDireita.setIcon(new javax.swing.ImageIcon(getClass().getResource("/projetofinaloo/imagem/NewSeguirParaDireita.png"))); // NOI18N
        jButtonSeguirParaDireita.setPreferredSize(new java.awt.Dimension(50, 50));

        jButtonSeguirParaEsquerda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/projetofinaloo/imagem/NewSeguirParaEsquerda.png"))); // NOI18N
        jButtonSeguirParaEsquerda.setPreferredSize(new java.awt.Dimension(50, 50));

        javax.swing.GroupLayout jPanelComandosMainLayout = new javax.swing.GroupLayout(jPanelComandosMain);
        jPanelComandosMain.setLayout(jPanelComandosMainLayout);
        jPanelComandosMainLayout.setHorizontalGroup(
            jPanelComandosMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelComandosMainLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelComandosMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(canvasSequenciasDeMovimentos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanelComandosMainLayout.createSequentialGroup()
                        .addComponent(jLabelTituloMain)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelComandosMainLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanelComandosMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jButtonPlay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanelComandosMainLayout.createSequentialGroup()
                                .addComponent(jButtonSeguirParaEsquerda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonVirarParaEsquerda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonSeguirParaFrente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonVirarParaDireita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonSeguirParaDireita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(50, 50, 50)))
                .addContainerGap())
        );
        jPanelComandosMainLayout.setVerticalGroup(
            jPanelComandosMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelComandosMainLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelTituloMain)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(canvasSequenciasDeMovimentos, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 16, Short.MAX_VALUE)
                .addGroup(jPanelComandosMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButtonSeguirParaDireita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonSeguirParaFrente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonVirarParaDireita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonVirarParaEsquerda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonSeguirParaEsquerda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButtonPlay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13))
        );

        getContentPane().add(jPanelComandosMain);
        jPanelComandosMain.setBounds(0, 200, 400, 300);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonPlayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPlayActionPerformed
        /**
         * Esse Botão irá a priore está testando se o personagem está movimentando
         * coso esteja correto ele aparecerá no quadrinho seguinte ao dele
         * 
         */
        
        
        //jLabelPersonagem.setBounds(450, 760, 30, 30);
        //Graphics g = null;        
        //Graphics2D graficos = (Graphics2D)g;
	//	graficos.drawImage((Image) JLabelTelaLaberinto.getIcon(), 0, 400, null);
	//	graficos.drawImage((Image) jLabelPersonagem.getIcon(), personagem.getMoveX(), personagem.getMoveY() , this);
        
        jLabelPersonagem.setLocation(450, 760);
        repaint();
        
        //jLabelPersonagem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/projetofinaloo/imagem/Back-SeguirFrente.gif")));
       // ImageIcon setaDireita = new ImageIcon("/projetofinaloo/imagem/NewSetaDireita.png");
        
        
    }//GEN-LAST:event_jButtonPlayActionPerformed

    private void jButtonSeguirParaFrenteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSeguirParaFrenteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButtonSeguirParaFrenteActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Fase1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Fase1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Fase1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Fase1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Fase1().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel JLabelAsh;
    private javax.swing.JLabel JLabelTelaLaberinto;
    private java.awt.Canvas canvasSequenciasDeMovimentos;
    private javax.swing.JButton jButtonPlay;
    private javax.swing.JButton jButtonSeguirParaDireita;
    private javax.swing.JButton jButtonSeguirParaEsquerda;
    private javax.swing.JButton jButtonSeguirParaFrente;
    private javax.swing.JButton jButtonVirarParaDireita;
    private javax.swing.JButton jButtonVirarParaEsquerda;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabelFotoPerfil;
    private javax.swing.JLabel jLabelNome;
    private javax.swing.JLabel jLabelNumeroPontos;
    private javax.swing.JLabel jLabelNumeroTentativas;
    private javax.swing.JLabel jLabelPersonagem;
    private javax.swing.JLabel jLabelPontuacao;
    private javax.swing.JLabel jLabelTentativas;
    private javax.swing.JLabel jLabelTextoNomeJogador;
    private javax.swing.JLabel jLabelTituloMain;
    private javax.swing.JPanel jPanelComandosMain;
    private javax.swing.JPanel jPanelInformacaoJogador;
    // End of variables declaration//GEN-END:variables

   
}
