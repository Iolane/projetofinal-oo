package controleP;

import java.awt.event.KeyEvent;


import com.sun.org.apache.xerces.internal.impl.io.ASCIIReader;

public class ControlePersonagem {
	
	public void moverPersonagem(){
		moveX += spaceX;
		moveY += spaceY;

	}
 
	//para eventos do teclado
	public void keyPessed (KeyEvent tecla){
		int input = tecla.getKeyCode();
		//ir para baixo
		if(input == KeyEvent.VK_UP){
			spaceY = 1;
		}
		if(input == KeyEvent.VK_DOWN){
			spaceY = -1;
		}
		if(input == KeyEvent.VK_RIGHT){
			spaceX = 1;
		}
		if(input == KeyEvent.VK_LEFT){
			spaceX = -1;
		}
	}
	
	//para eventos do teclado parado
		public void keyPessed (KeyEvent tecla){
			int input = tecla.getKeyCode();
			//ir para baixo
			if(input == KeyEvent.VK_UP){
				spaceY = 0;
			}
			if(input == KeyEvent.VK_DOWN){
				spaceY = 0;
			}
			if(input == KeyEvent.VK_RIGHT){
				spaceX = 0;
			}
			if(input == KeyEvent.VK_LEFT){
				spaceX = 0;
			}
		}
}
