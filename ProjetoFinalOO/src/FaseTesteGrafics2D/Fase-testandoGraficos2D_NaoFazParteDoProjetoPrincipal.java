package FaseTesteGrafics2D;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

import personagem.Personagem;
import controleP.ControlePersonagem;

public class Fase123 extends JPanel implements ActionListener{
	
	private Image cenario;
	private Personagem personagem;
	private ControlePersonagem umControle;
	private Timer timer;

	
	 public Fase123(){
		 
		addKeyListener(new TecladoAdapter());
	        ImageIcon imageCenario = new ImageIcon("recurso\\cenarioFase1.png");
	        cenario = imageCenario.getImage();
	        timer = new Timer(5, this);
	        //inicia o timer
	        timer.start();
	    }
	 
	 //para pintar na tela
	 public void pintarTela(Graphics pintar){
		 Graphics2D grafico = (Graphics2D) pintar;
		 grafico.drawImage(cenario, 0, 0, null);
		// grafico.drawImage(personagem.getImage(), umControle.getMoveX(), umControle.getMoveY(), this);
		 //para limpar/esquecer imagem anterior
		 pintar.dispose();
		 
	 }
	 
	 
	@Override
	/*public void actionPerformed(ActionEvent arg0) {
		
		umControle.moverPersonagem();
		//para thread
		repaint();
		
	}
	*/
	
	//OBS:TALVES SEJA MELHOR TRANFERIR ESSA PARTE PARA UM CONTROLE
	private class TecladoAdapter extends KeyAdapter{

		@Override
		public void keyPressed(KeyEvent t) {
		
			umControle.keyPessed(t);
		}

		@Override
		public void keyReleased(KeyEvent t) {
			
			umControle.keyReleased(t);
		}
		
		
	}
	 
}
