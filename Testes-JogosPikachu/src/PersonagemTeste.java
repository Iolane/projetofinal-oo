import javax.swing.ImageIcon;


public class PersonagemTeste {
	
	private String pontuacao;
	private String tentativas;
	private String nome;
	private ImageIcon iconPersonagem;
	private int personagemLocalX, personagemLocalY;
	private int sizePersonagemX, sizePersonagemY; 
	
	public PersonagemTeste(){
		
		this.nome = "Pikachu";
		this.pontuacao = "000";
		this.tentativas = "05";
		this.personagemLocalX = 760;
		this.personagemLocalY = 460;
		this.sizePersonagemX = 30;
		this.sizePersonagemY = 30;
		this.iconPersonagem = new ImageIcon(getClass().getResource("recursos/Teste1.gif"));
		
	}
	
	public ImageIcon getIconPersonagem() {
		return iconPersonagem;
	}

	public void setIconPersonagem(ImageIcon iconPersonagem) {
		this.iconPersonagem = iconPersonagem;
	}

	public int getPersonagemLocalX() {
		return personagemLocalX;
	}

	public void setPersonagemLocalX(int personagemLocalX) {
		this.personagemLocalX = personagemLocalX;
	}

	public int getPersonagemLocalY() {
		return personagemLocalY;
	}

	public void setPersonagemLocalY(int personagemLocalY) {
		this.personagemLocalY = personagemLocalY;
	}

	public int getSizePersonagemX() {
		return sizePersonagemX;
	}

	public void setSizePersonagemX(int sizePersonagemX) {
		this.sizePersonagemX = sizePersonagemX;
	}

	public int getSizePersonagemY() {
		return sizePersonagemY;
	}

	public void setSizePersonagemY(int sizePersonagemY) {
		this.sizePersonagemY = sizePersonagemY;
	}

	public String getPontuacao() {
		return pontuacao;
	}

	public void setPontuacao(String pontuacao) {
		this.pontuacao = pontuacao;
	}

	public String getTentativas() {
		return tentativas;
	}

	public void setTentativas(String tentativas) {
		this.tentativas = tentativas;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	
}

    
   