import java.awt.Color;

import javax.swing.ImageIcon;


public class Cenario {
	
	private Color corFundo;
	private ImageIcon fundoCenario;
	private ImageIcon iconImagemIrDireita;
	private ImageIcon iconImagemIrEsquerda;
	private ImageIcon iconImagemIrFrente;
	private ImageIcon iconImagemVirarDireita;
	private ImageIcon iconImagemVirarEsquerda;
	private ImageIcon iconImagemPlay;
	private ImageIcon iconImagemPerfil;
	private int sizeImagemPerfilX, sizeImagemPerfilY;
	private int posicaoImagemPerfilX, posicaoImagemPerfilY; 
	private int sizePainelInformacaoJogadorX, sizePainelInformacaoJogadorY;
	private int sizePainelComandosJogadorX, sizePainelComandosJogadorY;
	private int posicaoPainelInformacaoJogadorX, posicaoPainelInformacaoJogadorY; 
	private int posicaoPainelComandosJogadorX, posicaoPainelComandosJogadorY;
	private int sizeLabelLabirintoX, sizeLabelLabirintoY;
	private int posicaoLabelLabirintoX, posicaoLabelLabirintoY;
	
	public Cenario(){
		this.fundoCenario = new ImageIcon(getClass().getResource("recursos/NewFase1.png"));
		this.iconImagemPerfil = new ImageIcon(getClass().getResource("recursos/pikachu-perfil.gif"));
		this.iconImagemIrDireita = new ImageIcon(getClass().getResource("recursos/NewSeguirParaDireita.png"));
		this.iconImagemIrEsquerda= new ImageIcon(getClass().getResource("recursos/NewSeguirParaEsquerda.png"));
		this.iconImagemIrFrente = new ImageIcon(getClass().getResource("recursos/NewSetaParaFrente.png"));
		this.iconImagemVirarDireita = new ImageIcon(getClass().getResource("recursos/NewSetaParaDireita.png"));
		this.iconImagemVirarEsquerda = new ImageIcon(getClass().getResource("recursos/NewSetaParaEsquerda.png"));
		this.iconImagemPlay = new ImageIcon(getClass().getResource("recursos/iconePlay.png")) ;
		this.posicaoImagemPerfilX = 0;
		this.posicaoImagemPerfilY = 70;
		this.sizeImagemPerfilX = 180;
		this.sizeImagemPerfilY = 130;
		this.posicaoLabelLabirintoX = 400;
		this.posicaoLabelLabirintoY = 0;
		this.sizeLabelLabirintoX = 500;
		this.sizeLabelLabirintoY = 500;
		this.posicaoPainelInformacaoJogadorX = 0;
		this.posicaoPainelInformacaoJogadorY = 0;
		this.posicaoPainelComandosJogadorX = 0;
		this.posicaoPainelComandosJogadorY = 200;
		this.sizePainelComandosJogadorX = 400;
		this.sizePainelComandosJogadorY = 300;
	}
	
	public int getSizePainelInformacaoJogadorX() {
		return sizePainelInformacaoJogadorX;
	}
	public void setSizePainelInformacaoJogadorX(int sizePainelInformacaoJogadorX) {
		this.sizePainelInformacaoJogadorX = sizePainelInformacaoJogadorX;
	}
	public int getSizePainelInformacaoJogadorY() {
		return sizePainelInformacaoJogadorY;
	}
	public void setSizePainelInformacaoJogadorY(int sizePainelInformacaoJogadorY) {
		this.sizePainelInformacaoJogadorY = sizePainelInformacaoJogadorY;
	}
	public int getSizePainelComandosJogadorX() {
		return sizePainelComandosJogadorX;
	}
	public void setSizePainelComandosJogadorX(int sizePainelComandosJogadorX) {
		this.sizePainelComandosJogadorX = sizePainelComandosJogadorX;
	}
	public int getSizePainelComandosJogadorY() {
		return sizePainelComandosJogadorY;
	}
	public void setSizePainelComandosJogadorY(int sizePainelComandosJogadorY) {
		this.sizePainelComandosJogadorY = sizePainelComandosJogadorY;
	}
	public int getPosicaoPainelInformacaoJogadorX() {
		return posicaoPainelInformacaoJogadorX;
	}
	public void setPosicaoPainelInformacaoJogadorX(
			int posicaoPainelInformacaoJogadorX) {
		this.posicaoPainelInformacaoJogadorX = posicaoPainelInformacaoJogadorX;
	}
	public int getPosicaoPainelInformacaoJogadorY() {
		return posicaoPainelInformacaoJogadorY;
	}
	public void setPosicaoPainelInformacaoJogadorY(
			int posicaoPainelInformacaoJogadorY) {
		this.posicaoPainelInformacaoJogadorY = posicaoPainelInformacaoJogadorY;
	}
	public int getPosicaoPainelComandosJogadorX() {
		return posicaoPainelComandosJogadorX;
	}
	public void setPosicaoPainelComandosJogadorX(int posicaoPainelComandosJogadorX) {
		this.posicaoPainelComandosJogadorX = posicaoPainelComandosJogadorX;
	}
	public int getPosicaoPainelComandosJogadorY() {
		return posicaoPainelComandosJogadorY;
	}
	public void setPosicaoPainelComandosJogadorY(int posicaoPainelComandosJogadorY) {
		this.posicaoPainelComandosJogadorY = posicaoPainelComandosJogadorY;
	}

	public int getSizeLabelLabirintoX() {
		return sizeLabelLabirintoX;
	}

	public void setSizeLabelLabirintoX(int sizeLabelLabirintoX) {
		this.sizeLabelLabirintoX = sizeLabelLabirintoX;
	}

	public int getPosicaoLabelLabirintoX() {
		return posicaoLabelLabirintoX;
	}

	public void setPosicaoLabelLabirintoX(int posicaoLabelLabirintoX) {
		this.posicaoLabelLabirintoX = posicaoLabelLabirintoX;
	}

	public int getSizeLabelLabirintoY() {
		return sizeLabelLabirintoY;
	}

	public void setSizeLabelLabirintoY(int sizeLabelLabirintoY) {
		this.sizeLabelLabirintoY = sizeLabelLabirintoY;
	}

	public int getPosicaoLabelLabirintoY() {
		return posicaoLabelLabirintoY;
	}

	public void setPosicaoLabelLabirintoY(int posicaoLabelLabirintoY) {
		this.posicaoLabelLabirintoY = posicaoLabelLabirintoY;
	}

	public ImageIcon getIconImagemPerfil() {
		return iconImagemPerfil;
	}

	public void setIconImagemPerfil(ImageIcon iconImagemPerfil) {
		this.iconImagemPerfil = iconImagemPerfil;
	}

	public int getSizeImagemPerfilY() {
		return sizeImagemPerfilY;
	}

	public void setSizeImagemPerfilY(int sizeImagemPerfilY) {
		this.sizeImagemPerfilY = sizeImagemPerfilY;
	}

	public int getSizeImagemPerfilX() {
		return sizeImagemPerfilX;
	}

	public void setSizeImagemPerfilX(int sizeImagemPerfilX) {
		this.sizeImagemPerfilX = sizeImagemPerfilX;
	}

	public int getPosicaoImagemPerfilX() {
		return posicaoImagemPerfilX;
	}

	public void setPosicaoImagemPerfilX(int posicaoImagemPerfilX) {
		this.posicaoImagemPerfilX = posicaoImagemPerfilX;
	}

	public int getPosicaoImagemPerfilY() {
		return posicaoImagemPerfilY;
	}

	public void setPosicaoImagemPerfilY(int posicaoImagemPerfilY) {
		this.posicaoImagemPerfilY = posicaoImagemPerfilY;
	}

	public Color getCorFundo() {
		return corFundo;
	}

	public void setCorFundo(Color corFundo) {
		this.corFundo = corFundo;
	}

	public ImageIcon getIconImagemIrDireita() {
		return iconImagemIrDireita;
	}

	public void setIconImagemIrDireita(ImageIcon iconImagemIrDireita) {
		this.iconImagemIrDireita = iconImagemIrDireita;
	}

	public ImageIcon getIconImagemIrEsquerda() {
		return iconImagemIrEsquerda;
	}

	public void setIconImagemIrEsquerda(ImageIcon iconImagemIrEsquerda) {
		this.iconImagemIrEsquerda = iconImagemIrEsquerda;
	}

	public ImageIcon getIconImagemIrFrente() {
		return iconImagemIrFrente;
	}

	public void setIconImagemIrFrente(ImageIcon iconImagemIrFrente) {
		this.iconImagemIrFrente = iconImagemIrFrente;
	}

	public ImageIcon getIconImagemVirarDireita() {
		return iconImagemVirarDireita;
	}

	public void setIconImagemVirarDireita(ImageIcon iconImagemVirarDireita) {
		this.iconImagemVirarDireita = iconImagemVirarDireita;
	}

	public ImageIcon getIconImagemVirarEsquerda() {
		return iconImagemVirarEsquerda;
	}

	public void setIconImagemVirarEsquerda(ImageIcon iconImagemVirarEsquerda) {
		this.iconImagemVirarEsquerda = iconImagemVirarEsquerda;
	}

	public ImageIcon getIconImagemPlay() {
		return iconImagemPlay;
	}

	public void setIconImagemPlay(ImageIcon iconImagemPlay) {
		this.iconImagemPlay = iconImagemPlay;
	}

	public ImageIcon getFundoCenario() {
		return fundoCenario;
	}

	public void setFundoCenario(ImageIcon fundoCenario) {
		this.fundoCenario = fundoCenario;
	}
}
