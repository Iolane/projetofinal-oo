

import java.awt.Canvas;
import java.awt.
import java.awt.event.ActionListener;
import java.lang.Error;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
	

	public class fasesTestes extends JFrame{
		
		 	private javax.swing.JLabel JLabelTelaLaberinto;
		    private java.awt.Canvas canvasSequenciasDeMovimentos;
		    private javax.swing.JButton jButtonPlay;
		    private javax.swing.JButton jButtonSeguirParaDireita;
		    private javax.swing.JButton jButtonSeguirParaEsquerda;
		    private javax.swing.JButton jButtonSeguirParaFrente;
		    private javax.swing.JButton jButtonVirarParaDireita;
		    private javax.swing.JButton jButtonVirarParaEsquerda;
		    private javax.swing.JLabel jLabel5;
		    private javax.swing.JLabel jLabelFotoPerfil;
		    private javax.swing.JLabel jLabelNome;
		    private javax.swing.JLabel jLabelNumeroPontos;
		    private javax.swing.JLabel jLabelNumeroTentativas;
		    private javax.swing.JLabel jLabelPersonagem;
		    private javax.swing.JLabel jLabelPontuacao;
		    private javax.swing.JLabel jLabelTentativas;
		    private javax.swing.JLabel jLabelTextoNomeJogador;
		    private javax.swing.JLabel jLabelTituloMain;
		    private javax.swing.JPanel jPanelComandosMain;
		    private javax.swing.JPanel jPanelInformacaoJogador;
		   

		    PersonagemTeste umPersonagem;
		    Cenario umCenario;
	    
	      public fasesTestes(){
	 
	        //para titulo
	        super("Pikachu in... Lost on the Forest");
	        //para fechar janela
	        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        setSize(900,530);
	        //para não alterar o tamanho da janela
	        setResizable(false);
	        

	    	//instanciando conponentes
	        jPanelInformacaoJogador = new JPanel();
	        jLabelFotoPerfil = new JLabel();
	        jLabelNome = new JLabel();
	        jLabelPontuacao = new JLabel();
	        jLabelTentativas = new JLabel();
	        jLabel5 = new JLabel();
	        jLabelTextoNomeJogador = new JLabel();
	        jLabelNumeroPontos = new JLabel();
	        jLabelNumeroTentativas = new JLabel();
	        jLabelPersonagem = new JLabel();
	        JLabelTelaLaberinto = new JLabel();
	        jPanelComandosMain = new JPanel();
	        jButtonPlay = new JButton();
	        canvasSequenciasDeMovimentos = new Canvas();
	        jLabelTituloMain = new JLabel();
	        jButtonVirarParaEsquerda = new JButton();
	        jButtonSeguirParaFrente = new JButton();
	        jButtonVirarParaDireita = new JButton();
	        jButtonSeguirParaDireita = new JButton();
	        jButtonSeguirParaEsquerda = new JButton();
	        
	        //para fechar a janela
	        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
	        
	        //criar um container com layout nulo
	        getContentPane().setLayout(null);

	        //criar um painel para adicionar as infoema��es do jogador
	        jPanelInformacaoJogador.setBackground(new java.awt.Color(0, 0, 0));
	        jPanelInformacaoJogador.setMinimumSize(new java.awt.Dimension(400, 200));
	        jPanelInformacaoJogador.setLayout(null);

	        //foto do personagem no perfil
	        jLabelFotoPerfil.setIcon(umCenario.getIconImagemPerfil()); 
	        jPanelInformacaoJogador.add(jLabelFotoPerfil);
	        jLabelFotoPerfil.setBounds(umCenario.getPosicaoImagemPerfilX(), umCenario.getPosicaoImagemPerfilY(), umCenario.getSizeImagemPerfilX(), umCenario.getSizeImagemPerfilY());

	        //jLabel nome
	        jLabelNome.setFont(new java.awt.Font("OCR A Extended", 1, 18));
	        jLabelNome.setForeground(new java.awt.Color(255, 255, 255));
	        jLabelNome.setText("NOME:");
	        //adicionando no painel
	        jPanelInformacaoJogador.add(jLabelNome);
	        jLabelNome.setBounds(180, 80, 60, 20);

	        //JLabel pontuacao
	        jLabelPontuacao.setBackground(new java.awt.Color(0, 0, 0));
	        jLabelPontuacao.setFont(new java.awt.Font("OCR A Extended", 1, 18)); 
	        jLabelPontuacao.setForeground(new java.awt.Color(255, 255, 255));
	        jLabelPontuacao.setText("PONTUACAO:");
	        jPanelInformacaoJogador.add(jLabelPontuacao);
	        jLabelPontuacao.setBounds(180, 120, 120, 20);

	        //JLabel tentativas
	        jLabelTentativas.setBackground(new java.awt.Color(0, 0, 0));
	        jLabelTentativas.setFont(new java.awt.Font("OCR A Extended", 1, 18));
	        jLabelTentativas.setForeground(new java.awt.Color(255, 255, 255));
	        jLabelTentativas.setText("TENTAIVAS:");
	        jPanelInformacaoJogador.add(jLabelTentativas);
	        jLabelTentativas.setBounds(180, 160, 120, 20);

	        jLabel5.setBackground(new java.awt.Color(0, 0, 0));
	        jLabel5.setFont(new java.awt.Font("OCR A Extended", 1, 24)); 
	        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
	        jLabel5.setText("Informacoes do Jogador");
	        jPanelInformacaoJogador.add(jLabel5);
	        jLabel5.setBounds(30, 20, 360, 38);

	        //nome do jogador
	        jLabelTextoNomeJogador.setBackground(new java.awt.Color(0, 0, 0));
	        jLabelTextoNomeJogador.setFont(new java.awt.Font("OCR A Extended", 1, 18)); 
	        jLabelTextoNomeJogador.setForeground(new java.awt.Color(255, 255, 255));
	        jLabelTextoNomeJogador.setText(umPersonagem.getNome());
	        jPanelInformacaoJogador.add(jLabelTextoNomeJogador);
	        jLabelTextoNomeJogador.setBounds(240, 80, 110, 20);

	        //pontos do jogador
	        jLabelNumeroPontos.setBackground(new java.awt.Color(0, 0, 0));
	        jLabelNumeroPontos.setFont(new java.awt.Font("OCR A Extended", 1, 18)); 
	        jLabelNumeroPontos.setForeground(new java.awt.Color(255, 255, 255));
	        jLabelNumeroPontos.setText(umPersonagem.getPontuacao());
	        jPanelInformacaoJogador.add(jLabelNumeroPontos);
	        jLabelNumeroPontos.setBounds(310, 120, 24, 20);

	        //numero de tentativas
	        jLabelNumeroTentativas.setBackground(new java.awt.Color(0, 0, 0));
	        jLabelNumeroTentativas.setFont(new java.awt.Font("OCR A Extended", 1, 18)); 
	        jLabelNumeroTentativas.setForeground(new java.awt.Color(255, 255, 255));
	        jLabelNumeroTentativas.setText(umPersonagem.getTentativas());
	        jPanelInformacaoJogador.add(jLabelNumeroTentativas);
	        jLabelNumeroTentativas.setBounds(310, 160, 34, 20);

	       //adicionando e declarando dimens�es do painel de informa��es do usuario.
	        getContentPane().add(jPanelInformacaoJogador);
	        jPanelInformacaoJogador.setBounds(0, 0, 400, 200);

	        jLabelPersonagem.setBackground(new java.awt.Color(255, 255, 255));
	        jLabelPersonagem.setIcon(umPersonagem.getIconPersonagem()); 
	        getContentPane().add(jLabelPersonagem);
	        jLabelPersonagem.setBounds(umPersonagem.getPersonagemLocalX(), umPersonagem.getPersonagemLocalY(), umPersonagem.getSizePersonagemX(), umPersonagem.getSizePersonagemY());

	        JLabelTelaLaberinto.setIcon(umCenario.getFundoCenario()); 
	        getContentPane().add(JLabelTelaLaberinto);
	        JLabelTelaLaberinto.setBounds(400, 0, 500, 500);

	        jPanelComandosMain.setBackground(new java.awt.Color(0, 0, 0));
	        jPanelComandosMain.setMaximumSize(new java.awt.Dimension(50, 0));
	        jPanelComandosMain.setMinimumSize(new java.awt.Dimension(50, 50));
	        jPanelComandosMain.setPreferredSize(new java.awt.Dimension(50, 50));

	        jButtonPlay.setBackground(new java.awt.Color(0, 0, 0));
	        jButtonPlay.setIcon(new javax.swing.ImageIcon(getClass().getResource("/projetofinaloo/imagem/iconePlay.png"))); 
	        jButtonPlay.setPreferredSize(new java.awt.Dimension(50, 50));
	        jButtonPlay.addActionListener(new java.awt.event.ActionListener() {
	            public void actionPerformed(java.awt.event.ActionEvent evt) {
	                jButtonPlayActionPerformed(evt);
	            }
	        });

	        canvasSequenciasDeMovimentos.setBackground(new java.awt.Color(51, 51, 51));

	        jLabelTituloMain.setBackground(new java.awt.Color(0, 0, 0));
	        jLabelTituloMain.setFont(new java.awt.Font("OCR A Extended", 1, 18)); 
	        jLabelTituloMain.setForeground(new java.awt.Color(255, 255, 255));
	        jLabelTituloMain.setText("MAIN:");

	        jButtonVirarParaEsquerda.setIcon(umCenario.getIconImagemVirarEsquerda()); 
	        jButtonVirarParaEsquerda.setPreferredSize(new java.awt.Dimension(50, 50));

	        jButtonSeguirParaFrente.setIcon(umCenario.getIconImagemIrFrente()); 
	        jButtonSeguirParaFrente.setPreferredSize(new java.awt.Dimension(50, 50));
	        jButtonSeguirParaFrente.addActionListener(new java.awt.event.ActionListener() {
	            public void actionPerformed(java.awt.event.ActionEvent evt) {
	                jButtonSeguirParaFrenteActionPerformed(evt);
	            }
	        });

	        jButtonVirarParaDireita.setIcon(umCenario.getIconImagemVirarDireita()); 
	        jButtonVirarParaDireita.setPreferredSize(new java.awt.Dimension(50, 50));

	        jButtonSeguirParaDireita.setIcon(umCenario.getIconImagemIrDireita()); 
	        jButtonSeguirParaDireita.setPreferredSize(new java.awt.Dimension(50, 50));

	        jButtonSeguirParaEsquerda.setIcon(umCenario.getIconImagemIrEsquerda()); 
	        jButtonSeguirParaEsquerda.setPreferredSize(new java.awt.Dimension(50, 50));

	        javax.swing.GroupLayout jPanelComandosMainLayout = new javax.swing.GroupLayout(jPanelComandosMain);
	        jPanelComandosMain.setLayout(jPanelComandosMainLayout);
	        jPanelComandosMainLayout.setHorizontalGroup(
	            jPanelComandosMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(jPanelComandosMainLayout.createSequentialGroup()
	                .addContainerGap()
	                .addGroup(jPanelComandosMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addComponent(canvasSequenciasDeMovimentos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                    .addGroup(jPanelComandosMainLayout.createSequentialGroup()
	                        .addComponent(jLabelTituloMain)
	                        .addGap(0, 0, Short.MAX_VALUE))
	                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelComandosMainLayout.createSequentialGroup()
	                        .addGap(0, 0, Short.MAX_VALUE)
	                        .addGroup(jPanelComandosMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
	                            .addComponent(jButtonPlay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                            .addGroup(jPanelComandosMainLayout.createSequentialGroup()
	                                .addComponent(jButtonSeguirParaEsquerda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                                .addComponent(jButtonVirarParaEsquerda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                                .addComponent(jButtonSeguirParaFrente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
	                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                        .addComponent(jButtonVirarParaDireita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                        .addComponent(jButtonSeguirParaDireita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                        .addGap(50, 50, 50)))
	                .addContainerGap())
	        );
	        jPanelComandosMainLayout.setVerticalGroup(
	            jPanelComandosMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(jPanelComandosMainLayout.createSequentialGroup()
	                .addContainerGap()
	                .addComponent(jLabelTituloMain)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addComponent(canvasSequenciasDeMovimentos, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 16, Short.MAX_VALUE)
	                .addGroup(jPanelComandosMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addComponent(jButtonSeguirParaDireita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                    .addComponent(jButtonSeguirParaFrente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                    .addComponent(jButtonVirarParaDireita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                    .addComponent(jButtonVirarParaEsquerda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                    .addComponent(jButtonSeguirParaEsquerda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                .addComponent(jButtonPlay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addGap(13, 13, 13))
	        );

	        getContentPane().add(jPanelComandosMain);
	        jPanelComandosMain.setBounds(0, 200, 400, 300);

	        pack();
	    }

	    private void jButtonPlayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPlayActionPerformed
	        // TODO add your handling code here:
	        //jLabelPersonagem.setBounds(450, 760, 30, 30);
	        //Graphics g = null;        
	        //Graphics2D graficos = (Graphics2D)g;
		//	graficos.drawImage((Image) JLabelTelaLaberinto.getIcon(), 0, 400, null);
		//	graficos.drawImage((Image) jLabelPersonagem.getIcon(), personagem.getMoveX(), personagem.getMoveY() , this);
	        
	        //jLabelPersonagem.setLocation(450, 760);
	        //repaint();
	        
	        //jLabelPersonagem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/projetofinaloo/imagem/Back-SeguirFrente.gif")));
	       // ImageIcon setaDireita = new ImageIcon("/projetofinaloo/imagem/NewSetaDireita.png");
	        
	        
	    }//GEN-LAST:event_jButtonPlayActionPerformed

	    private void jButtonSeguirParaFrenteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSeguirParaFrenteActionPerformed
	        // TODO add your handling code here:
	    }//GEN-LAST:event_jButtonSeguirParaFrenteActionPerformed

	    public static void main(String[] args) {
	        
	       try {
	            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
	                if ("Nimbus".equals(info.getName())) {
	                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
	                    break;
	                }
	            }
	        } catch (ClassNotFoundException ex) {
	            java.util.logging.Logger.getLogger(fasesTestes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
	        } catch (InstantiationException ex) {
	            java.util.logging.Logger.getLogger(fasesTestes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
	        } catch (IllegalAccessException ex) {
	            java.util.logging.Logger.getLogger(fasesTestes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
	        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
	            java.util.logging.Logger.getLogger(fasesTestes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
	        }
	        //</editor-fold>

	        /* Create and display the form */
	       java.awt.EventQueue.invokeLater(new Runnable() {
	           public void run() {
	                new fasesTestes().setVisible(true);
	            }
	        });
	   }

	    
	   
	   
	

}
